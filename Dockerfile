FROM node:14-alpine
WORKDIR /app
COPY index.js ./
COPY package.json ./
RUN yarn install
EXPOSE 3000
CMD ["yarn", "run", "start"]
