# landx-pingpong-app

How I finish the test. This is the steps:
1. Clone pingpong-app code from gitlab.landx.id to local and push the code to gitlab.com (https://gitlab.com/h4nz3n/landx-pingpong-app.git)

2. Test the code to check how the code works locally.

3. Build the app image and test the image locally. Dockerfile located in this repository.

4. Make automation code (.gitlab-ci.yml) located in root repository to build and push the image to Dockerhub.

5. Setup gitlab CI/CD variable that used in gitlab-ci.yml.
  In the gitlab menu, choose Settings > CI/CD > Variables choose add variable
  Add variable and fill the value as needed CI_REGISTRY_USER(this is username Dockerhub), CI_REGISTRY_PASSWORD  (this is password for Dockerhub, CI_REGISTRY(fill with docker.io for Dockerhub image registry), REGISTRY_IMAGE(fill with index.docker.io/<username>/<projectname>
  
6. Install docker, minikube, and kubectl locally (laptop ).

7. Make yaml file to deploy pingpong-app to minikube cluster.
   Make yaml file (deployment.yaml located in this repository) and execute the deployment.yaml to deploy the    pingpong-app to minikube cluster (kubectl apply -f deployment.yaml).

8. Check the minikube status and ensure cluster running and test the pingpong-app function from browser.

[For more complete explanation.](https://tinyurl.com/5f9rsbk3)


